package com.warathip.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class HelloMyName extends JFrame{
    JLabel lblName;
    JTextField txtName;
    JButton btnHello;
    JLabel lblHello;
    public HelloMyName() {
        super("Hello My Name");
        lblName = new JLabel("Name : ");
        lblName.setBounds(10,10,100,20);
        txtName = new JTextField();
        txtName.setBounds(70,10,200,20);
        btnHello = new JButton("Hello");
        btnHello.setBounds(30,40,250,20);
        btnHello.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String myName = txtName.getText();
                lblHello.setText("Hello "+ myName);
            }
            
        });
        lblHello = new JLabel("Hello My Name");
        lblHello.setBounds(30,70,250,20);
        lblHello.setHorizontalAlignment(JLabel.CENTER);
        this.add(lblHello);
        this.add(btnHello);
        this.add(txtName);
        this.add(lblName);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setSize(400,300);
    }
    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();
    }
}
